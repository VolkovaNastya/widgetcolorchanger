colorChange = function () {
    var widget = this;
    this.code = null;

    
    this.bind_actions = function () {
        
    };

    
    this.render = function () {

        let color = document.querySelectorAll('.pipeline_cell')[2]
        .querySelector('.pipeline_status__head_line').style.background

        document.querySelectorAll('.pipeline_cell')[2]
        .querySelector('.pipeline_status__head_title').style.color = color
    
    };

    
    this.init = function () {

    };

    
    this.bootstrap = function (code) {
        widget.code = code;
        
        var status = 1;

        if (status) {
            widget.init();
            widget.render();
            widget.bind_actions();
            $(document).on('widgets:load', function () {
                widget.render();
            });
        }
    }
};


yadroWidget.widgets['color-change'] = new colorChange();
yadroWidget.widgets['color-change'].bootstrap('color-change');